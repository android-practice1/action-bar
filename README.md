# ActionBar

1. Create Menu directory & XML file

 * Create a menu directory in the `res/menu` folder. If you can't do it, then try with **Project View** mode. 
 * Create a **Menu Resource File** and File name as `res/menu/main_menu.xml`

2. Working with `menu_item`

Add `Menu Item` from design Palette. 
<strong>XML Design Flow</strong>

> You can select a Action Bar

```xml
<item
  android:id="@+id/action_settings"
  android:title="Settings"
  app:showAsAction="never">
</item>
```

3. Work on `MainActivity`

And get sure to override the `onCreateOptionsMenu(Menu menu)` in the `MainActivity` class like this:

```java
// menu on action bar
@Override
public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_item,menu);
    return true;
}
```

4. To work under menu 

```java
// on menu option
@Override
public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    if (id == R.id.action_settings) {
        return true;
    }

    return super.onOptionsItemSelected(item);
}
```

### TESTED AND OK

Farhan Sadik
