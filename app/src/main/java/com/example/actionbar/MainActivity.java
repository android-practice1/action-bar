package com.example.actionbar;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);
        textView.setText("Hello World");
    }

    // menu on action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    // on menu option
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // action bar menu
        if (id == R.id.coution) {
            Toast.makeText(getApplicationContext(),"Clicked on coution", Toast.LENGTH_LONG).show();
            return true;
        } else if (id == R.id.sendSMS) {
            Toast.makeText(getApplicationContext(), "clicked on send sms", Toast.LENGTH_LONG).show();
            return true;
        }

        // dot
        switch (item.getItemId()){
            case R.id.action_settings:
                Toast.makeText(getApplicationContext(),"Clicked on settings", Toast.LENGTH_LONG).show();
                break;
            case R.id.aboutmenuitem:
                Toast.makeText(this, "Clicked on about menu", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}